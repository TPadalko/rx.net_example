######Warning! Project is not finished yet,it's just a raw presentation of a general idea

https://developers.redhat.com/blog/wp-content/uploads/2017/06/reactive-programming.png

Reactive means responsive which basically applies very well in real world.
Reactive Programming allows you to build complex requests which are asynchronous 
and result comes right where you need it without blocking requests one by one.

This architecture is an implementation of MVI (Event based) with clean architecture for connecting RX.NET streams.
MVI is the most useful case for RX because RX is an event based reactive library 
and by combining with defined input(*X*ControllerModel) and output(*X*ViewModel) models 
you get centralized stream manager(*X*EventManager).

##**Project Structure**

###`Models` 
ControllerModels -> Event model sent from UI/View to EventManager through Controller
ViewModels -> Result model with collected data required for UI/View

Each of these models has its own nested classes which contains only required data for exact event. 
In this way you restrict to use only the data needed for particular task. By using one class which 
contains every possible field for a block (in my example which are Book and Author MVCs) you get not 
completely filled model and you can simply try to get not existing data.


###`ReactiveEventsManagers`
EventManager is responsible for detecting received events proceeding to the between services to create 
stream of requests.
BaseEventManager.GetStream(ControllerModel) configures stream to calculate and observe for result in required threads.

Note: Observable.SelectMany is a method connecting to another Observable in current stream.
                .Select is a method for transforming data.

###`Service` 
Services are responsible for making calls to external api to retrieve data, cache it and if available in 
cache retrieve from it without making unnecessary call.


Why RX?

Reactive Extensions are about sequences. With sequences we can create projections, transformations and filters.
Reactive Extensions can get pretty complex and is not always intuitive, but you can create some elegant solutions with it.