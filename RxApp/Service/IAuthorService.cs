using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RxApp.domain;

namespace RxApp.Service
{
    public interface IAuthorService
    {
        IObservable<Author> FindById(int id);
        IObservable<List<Author>> FindAll(string name);
    }
}