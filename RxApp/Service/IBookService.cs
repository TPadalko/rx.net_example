using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RxApp.domain;

namespace RxApp.Service
{
    public interface IBookService
    {
        IObservable<Book> FindById(int id);
        IObservable<List<Book>> FindAll();
    }
}