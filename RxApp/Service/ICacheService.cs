using RxApp.domain;

namespace RxApp.Service
{
    public interface ICacheService
    {
        void CacheBook(Book book);

        Book RetrieveBookFromCache(int id);
        
        void CacheAuthor(Author author);
 
        Author RetrieveAuthorFromCache(int id);
    }
}