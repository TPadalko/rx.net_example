using Microsoft.Extensions.Caching.Memory;
using RxApp.domain;

namespace RxApp.Service
{
    public class CacheService : ICacheService
    {
        private IMemoryCache _memoryCache { get; }

        public CacheService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public void CacheBook(Book book)
        {
            var key = "book" + book.Id;
            _memoryCache.Set(key, book);
        }

        public Book RetrieveBookFromCache(int id)
        {
            var key = "book" + id;
            _memoryCache.TryGetValue(key, out Book value);
            return value;
        }

        public void CacheAuthor(Author author)
        {
            var key = "author" + author.Id;
            _memoryCache.Set(key, author);
        }

        public Author RetrieveAuthorFromCache(int id)
        {
            var key = "author" + id;
            _memoryCache.TryGetValue(key, out Author value);
            return value;
        }
    }
}