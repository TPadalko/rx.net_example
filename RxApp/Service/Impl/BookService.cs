using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using RestEase;
using RxApp.Apis;
using RxApp.domain;
using RxApp.Models.ControllerModels;
using RxApp.Models.ViewModels;

namespace RxApp.Service
{
    public class BookService : IBookService
    {
        private IBookApi _api { get; }
        private ICacheService _cacheService { get; }

        public BookService(ICacheService cacheService)
        {
            _api = RestClient.For<IBookApi>("http://localhost:60080/api");
            _cacheService = cacheService;
        }

        public IObservable<Book> FindById(int id)
        {
            var book = _cacheService.RetrieveBookFromCache(id);
            if (book != null)
            {
                return Observable.Return(book);
            }

            return Observable.FromAsync(_api.GetBook)
                .SubscribeOn(new NewThreadScheduler(ts => new Thread(ts)))
                .Select(b =>
            {
                _cacheService.CacheBook(b);
                return b;
            });
        }

        public IObservable<List<Book>> FindAll()
        {
            return Observable.FromAsync(_api.GetAllBooks)
                .SubscribeOn(new NewThreadScheduler(ts => new Thread(ts)));
        }
    }
}