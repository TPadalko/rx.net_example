using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using RestEase;
using RxApp.Apis;
using RxApp.domain;
using RxApp.Models.ControllerModels;
using RxApp.Models.ViewModels;

namespace RxApp.Service
{
    public class AuthorService : IAuthorService
    {
        private IAuthorApi _api { get; }
        private ICacheService _cacheService { get; }


        public AuthorService(ICacheService cacheService)
        {
            _api = RestClient.For<IAuthorApi>("http://localhost:60080/api");
            _cacheService = cacheService;
        }

        public IObservable<Author> FindById(int id)
        {
            var author = _cacheService.RetrieveAuthorFromCache(id);
            if (author != null)
            {
                return Observable.Return(author);
            }

            return Observable.FromAsync(_api.GetAuthor).Select(a =>
            {
                _cacheService.CacheAuthor(a);
                return a;
            });
        }

        public IObservable<List<Author>> FindAll(string name)
        {
            return Observable.FromAsync(_api.GetAllAuthors);
        }
    }
}