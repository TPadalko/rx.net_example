using System.Collections.Generic;
using System.Threading.Tasks;
using RestEase;
using RxApp.domain;

namespace RxApp.Apis
{
    public interface IAuthorApi
    {
        [Get("author/allAuthors")]
        Task<List<Author>> GetAllAuthors();
        
        [Get("/author/byId")]
        Task<Author> GetAuthor();
    }
}