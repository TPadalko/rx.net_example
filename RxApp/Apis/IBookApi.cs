using System.Collections.Generic;
using System.Threading.Tasks;
using RestEase;
using RxApp.domain;

namespace RxApp.Apis
{
    public interface IBookApi
    {
        [Get("book/allBooks")]
        Task<List<Book>> GetAllBooks();
        
        [Get("/book/byId")]
        Task<Book> GetBook();
    }
}