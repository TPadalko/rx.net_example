using System;

namespace RxApp.Models
{
    public class ErrorViewModel
    {
        public string Message { get; set; }
        public Exception Exception { get; set; }

        public bool ShowMessage => !string.IsNullOrEmpty(Message);
    }
}