using Newtonsoft.Json;

namespace RxApp.domain
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public int Author { get; set; }
        
        [JsonIgnore]
        public string AuthorsName { get; set; }
    }
}