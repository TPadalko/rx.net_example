namespace RxApp.Models.ControllerModels
{
    public class AuthorControllerModel : BaseControllerModel
    {
        public class ById : AuthorControllerModel
        {
            public int Id { get; set; }
        }
        
        public class All : AuthorControllerModel
        {
            public string Name { get; set; }
        }
    }
}