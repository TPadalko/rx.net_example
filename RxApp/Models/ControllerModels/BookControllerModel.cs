namespace RxApp.Models.ControllerModels
{
    public class BookControllerModel : BaseControllerModel
    {
        
        public class ById : BookControllerModel
        {
            public int Id { get; set; }
        }
        
        public class All : BookControllerModel
        {
            public string Title { get; set; }
            public double Year { get; set; }
        }
    }
}