namespace RxApp.Models.ViewModels
{
    public class AuthorViewModel : BaseViewModel
    {
        public class Loading : AuthorViewModel
        { 
            
        }

        public class SetAuthor : AuthorViewModel
        {
            public int Id { get; private set; }
            public string Name { get; private set; }
        }    
    }
}