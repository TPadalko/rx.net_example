using System;
using System.Collections.Generic;
using RxApp.domain;

namespace RxApp.Models.ViewModels
{
    public class BookViewModel : BaseViewModel
    {
        public class Loading : BookViewModel
        { 
            
        }

        public class SetBook : BookViewModel
        {
            public Book book { get; set; }
        }
        
        public class SetBooks : BookViewModel
        {
            public List<Book> books { get; set; }
        }
    }
}