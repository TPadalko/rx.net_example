using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RxApp.Models.ControllerModels;
using RxApp.ReactiveEventsManagers;
using RxApp.Service;

namespace RxApp.Controllers
{
    [Route("/books")]
    public class BooksController : Controller
    {
        private BooksEventManager _booksEventManager { get; }
        private IBookService _bookService { get; }


        public BooksController(BooksEventManager booksEventManager, IBookService bookService)
        {
            _booksEventManager = booksEventManager;
            _bookService = bookService;
        }


        public async Task<IActionResult> Books()
        {
            var sw = new Stopwatch();

            GCReset();
            sw.Restart();

            BookControllerModel controllerModel = new BookControllerModel.All();

            var msg = await _booksEventManager.GetStream(controllerModel);

            sw.Stop();
            Console.WriteLine("FromAsync: {0}", sw.ElapsedMilliseconds);

            GCReset();
            sw.Restart();

            ViewData["AllBooks"] = msg;
            return View();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Book(int id)
        {
            var sw = new Stopwatch();

            GCReset();
            sw.Restart();

            BookControllerModel controllerModel = new BookControllerModel.ById {Id = id};

            var msg = await _booksEventManager.GetStream(controllerModel);

            sw.Stop();
            Console.WriteLine("FromAsync: {0}", sw.ElapsedMilliseconds);

            GCReset();
            sw.Restart();

            ViewData["Message"] = msg;
            return View();
        }

        private static void GCReset()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}