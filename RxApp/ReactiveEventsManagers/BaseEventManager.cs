using System;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using RxApp.Models.ControllerModels;
using RxApp.Models.ViewModels;

namespace RxApp.ReactiveEventsManagers
{
    public abstract class BaseEventManager<CM, VM>
        where CM : BaseControllerModel
        where VM : BaseViewModel
    {
        public Task<VM> GetStream(CM controllerModel)
        {
            return ObserveEvents(controllerModel).ToTask();
        }

        protected abstract IObservable<VM> ObserveEvents(CM controllerModel);
    }
}