using System;
using System.Reactive.Linq;
using Microsoft.Extensions.Logging;
using RxApp.Models.ControllerModels;
using RxApp.Models.ViewModels;
using RxApp.Service;

namespace RxApp.ReactiveEventsManagers
{
    public class BooksEventManager : BaseEventManager<BookControllerModel, BookViewModel>
    {
        private IBookService _bookService { get; }
        private IAuthorService _authorService { get; }
        private ILogger _logger;

        public BooksEventManager(IBookService bookService, IAuthorService authorService, ILoggerFactory loggerFactory)
        {
            _bookService = bookService;
            _authorService = authorService;
            _logger = loggerFactory.CreateLogger<BooksEventManager>();

        }

        protected override IObservable<BookViewModel> ObserveEvents(BookControllerModel controllerModel)
        {
            switch (controllerModel)
            {
                case BookControllerModel.ById getBook:
                    return _bookService.FindById(getBook.Id)
                        //SelectMany is a very silly name for a flatMap()
                        .SelectMany(book => _authorService.FindById(book.Author)
                            .Select(author =>
                            {
                                book.AuthorsName = author.Name;
                                return book;
                            }))
                        .Select(book => new BookViewModel.SetBook {book = book})
                        .Catch<BookViewModel, Exception>(exception =>
                        {
                            //TODO proper error handling
                            _logger.LogCritical("BooksEventManager.ObserveEvents().GetBook " + exception);
                            return Observable.Empty<BookViewModel.Loading>();
                        });

                case BookControllerModel.All allBooks:
                    return _bookService.FindAll()
                        .Select(books => new BookViewModel.SetBooks {books = books})
                        .Catch<BookViewModel, Exception>(exception =>
                        {
                            _logger.LogCritical("BooksEventManager.ObserveEvents().AllBooks " + exception);
                            return Observable.Empty<BookViewModel.Loading>();
                        });

                default:
                    throw new ArgumentOutOfRangeException(controllerModel.GetType().Name);
            }
        }
    }
}