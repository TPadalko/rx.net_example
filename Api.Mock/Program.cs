﻿using System;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;

namespace Api.Mock
{
    public static class Program
    {
        private static void Main()
        {
            JsonConvert.DefaultSettings = () =>
                new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };

            var server = FluentMockServer.Start(60080);

	        const string bookApi = "book";
	        const string authorApi = "author";

            //GET
            AddGetFromFile(server, bookApi, "allBooks");
	        AddGetFromFile(server, bookApi, "byId");
	        
            AddGetFromFile(server, authorApi,"allAuthors");
	        AddGetFromFile(server, authorApi, "byId");

			//POST
			AddPostFromFile(server, "new");
			
			Console.WriteLine("Press any key to stop the server");
            Console.Read();
        }

        private static void AddGetFromFile(FluentMockServer server, string apiType, string apiName, HttpStatusCode? withStatusCode = null)
        {
			var request = Request.Create()
				.WithPath($"/api/{apiType}/{apiName}")
				.UsingGet();

			var response = Response.Create()
				.WithDelay(200)
				.WithHeader("Content-Type", "application/json")
				.WithBodyFromFile($@"json/{apiType}/{apiName}.json", cache: false);

			if (withStatusCode.HasValue)
			{
				response.WithStatusCode(withStatusCode.Value);
			}

			server.Given(request).RespondWith(response);
        }

		private static void AddPostFromFile(FluentMockServer server, string apiName, HttpStatusCode? withStatusCode = null)
		{
			var request = Request.Create()
				.WithPath($"/api/{apiName}")
				.UsingPost();

			var response = Response.Create()
				.WithDelay(200)
				.WithHeader("Content-Type", "application/json")
				.WithBodyFromFile($@"json/{apiName}.json", cache: false);

			if (withStatusCode.HasValue)
			{
				response.WithStatusCode(withStatusCode.Value);
			}

			var mock = server.Given(request);
			mock.RespondWith(response);
        }
    }
}